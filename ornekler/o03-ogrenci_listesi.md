* Kullanıcıdan en başta bir işlem yapmasını isteyeceksiniz
* İstenilen işlemler: `list, add, select, quit`
* `list` işleminde sistemde girilmiş olan öğrenciler, liste indeksi ile beraber ekrana yazdırılacak.
* `add` işleminde listeye yeni bir öğrenci eklenecek.
* `select` işleminde indexi belirtilen öğrenci seçilip ekrana yazdırılacak.
* `quit` işleminde programdan çıkış yapılacak.
* Öğrenci listesi içerisindeki ögeler şu içerikleri barındıracak: `name, surname, age, classroom`
* Öğrenciler bir liste içerisinde dict olarak tutulacak.
* `list` komutu esnasında ekrana şu şekilde yazdırılacak: `Mahmut Tuncer - 46 - C301`
* `quit` hariç herhangi bir işlemin ardından program en başa dönecektir.
